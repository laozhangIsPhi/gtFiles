-- --------------------------------------------------------
-- 主机:                           118.25.251.113
-- 服务器版本:                        8.0.21 - MySQL Community Server - GPL
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 blogTest001 的数据库结构
CREATE DATABASE IF NOT EXISTS `blogTest001` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `blogTest001`;

-- 导出  表 blogTest001.Advertisement 结构
CREATE TABLE IF NOT EXISTS `Advertisement` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ImgUrl` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Createdate` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Advertisement 的数据：~0 rows (大约)
DELETE FROM `Advertisement`;
/*!40000 ALTER TABLE `Advertisement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Advertisement` ENABLE KEYS */;

-- 导出  表 blogTest001.BlogArticle 结构
CREATE TABLE IF NOT EXISTS `BlogArticle` (
  `bID` int NOT NULL AUTO_INCREMENT,
  `bsubmitter` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `btitle` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `bcategory` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `bcontent` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `btraffic` int NOT NULL,
  `bcommentNum` int NOT NULL,
  `bUpdateTime` datetime NOT NULL,
  `bCreateTime` datetime NOT NULL,
  `bRemark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.BlogArticle 的数据：~1 rows (大约)
DELETE FROM `BlogArticle`;
/*!40000 ALTER TABLE `BlogArticle` DISABLE KEYS */;
INSERT INTO `BlogArticle` (`bID`, `bsubmitter`, `btitle`, `bcategory`, `bcontent`, `btraffic`, `bcommentNum`, `bUpdateTime`, `bCreateTime`, `bRemark`, `IsDeleted`) VALUES
	(1, 'admin', '测试数据：IIS new add website ，some wrong:The requested page cannot be accessed because the related configuration data for the page is invalid.', '技术博文', '                            <p>问题:</p><h1><a href="https://www.cnblogs.com/yipeng-yu/p/6210380.html">The requested page cannot be accessed because the related configuration data for the page is invalid.</a></h1><p>HTTP Error 500.19 - Internal Server Error The requested page cannot be accessed because the related configuration data for the page is invalid.</p><p>Detailed Error Information:</p><p>Module IIS Web Core</p><p>Notification Unknown</p><p>Handler Not yet determined</p><p>Error Code 0x80070003</p><p>Config Error Cannot read configuration file</p><p>Config File \\?\\D:\\Projects\\...\\web.config</p><p>Requested URL http:// localhost:8080/</p><p>Physical Path</p><p>Logon Method Not yet determined</p><p>Logon User Not yet determined</p><p>Request Tracing Directory C:\\Users\\...\\TraceLogFiles\\</p><p>Config Source:</p><p>Answer:</p><p>1，find the site\'s application pools</p><p>2,"Advanced Settings" ==&gt; Indentity ==&gt;&nbsp; Custom account</p><p><br></p><p><br></p>', 127, 1, '2019-01-01 00:00:00', '2019-01-01 00:00:00', NULL, b'0');
/*!40000 ALTER TABLE `BlogArticle` ENABLE KEYS */;

-- 导出  表 blogTest001.Guestbook 结构
CREATE TABLE IF NOT EXISTS `Guestbook` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `blogId` int NOT NULL,
  `createdate` datetime NOT NULL,
  `username` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `phone` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `QQ` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `body` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ip` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `isshow` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Guestbook 的数据：~0 rows (大约)
DELETE FROM `Guestbook`;
/*!40000 ALTER TABLE `Guestbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `Guestbook` ENABLE KEYS */;

-- 导出  表 blogTest001.ModulePermission 结构
CREATE TABLE IF NOT EXISTS `ModulePermission` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  `ModuleId` int NOT NULL,
  `PermissionId` int NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.ModulePermission 的数据：~0 rows (大约)
DELETE FROM `ModulePermission`;
/*!40000 ALTER TABLE `ModulePermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModulePermission` ENABLE KEYS */;

-- 导出  表 blogTest001.Modules 结构
CREATE TABLE IF NOT EXISTS `Modules` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LinkUrl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Area` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Controller` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Action` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `OrderSort` int NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsMenu` bit(1) NOT NULL,
  `Enabled` bit(1) NOT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  `ParentId` int DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Modules 的数据：~42 rows (大约)
DELETE FROM `Modules`;
/*!40000 ALTER TABLE `Modules` DISABLE KEYS */;
INSERT INTO `Modules` (`Id`, `IsDeleted`, `Name`, `LinkUrl`, `Area`, `Controller`, `Action`, `Icon`, `Code`, `OrderSort`, `Description`, `IsMenu`, `Enabled`, `CreateId`, `CreateBy`, `CreateTime`, `ModifyId`, `ModifyBy`, `ModifyTime`, `ParentId`) VALUES
	(1, b'0', 'values接口', '/api/values', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(2, b'0', 'claims的接口', '/api/claims', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(3, b'0', 'UserRole接口', '/api/UserRole', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(4, b'0', NULL, '/api/v2/Apb/apbs', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(5, b'0', '修改 tibug 文章', '/api/TopicDetail/update', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(6, b'0', '删除tibug文章', '/api/TopicDetail/delete', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(7, b'0', '获取用户', '/api/user/get', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(8, b'0', '获取用户详情', '/api/user/get/\\d+', NULL, NULL, NULL, NULL, NULL, 1, NULL, b'0', b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(9, b'1', '角色接口', '/api/role', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(10, b'0', '添加用户', '/api/user/post', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(11, b'0', '删除用户', '/api/user/delete', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(12, b'0', '修改用户', '/api/user/put', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(13, b'0', '获取api接口', '/api/module/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(14, b'0', '删除api接口', '/api/module/delete', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(15, b'0', '修改api接口', '/api/module/put', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(16, b'0', '添加api接口', '/api/module/post', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(17, b'0', '获取菜单', '/api/permission/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(18, b'0', '删除菜单', '/api/permission/delete', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(19, b'0', '修改菜单', '/api/permission/put', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(20, b'0', '添加菜单', '/api/permission/post', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(21, b'0', '获取菜单树', '/api/permission/getpermissiontree', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(22, b'0', '获取角色', '/api/role/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(23, b'0', '删除角色', '/api/role/delete', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(24, b'0', '修改角色', '/api/role/put', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(25, b'0', '添加角色', '/api/role/post', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(26, b'0', '获取bug', '/api/TopicDetail/Get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(27, b'0', '获取博客', '/api/Blog', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(28, b'0', '保存分配', '/api/permission/Assign', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(29, b'0', 'Get导航条', '/api/permission/GetNavigationBar', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(30, b'1', 'test', '/api/Blog/delete1', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(31, b'1', 'test', '/api/Blog/delete2', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(32, b'0', '删除博客', '/api/Blog/delete', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(33, b'0', '获取全部日志', '/api/Monitor/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(34, b'1', 'Agent -测试- 快速添加接口权限', '/api/Agent/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(35, b'1', 'test', '/api/test/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(36, b'0', 'Department - 测试新建api -  部门管控', '/api/Department/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(37, b'0', '获取任务调取分页', '/api/TasksQz/get', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(38, b'0', '添加任务', '/api/TasksQz/Post', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(39, b'0', '编辑任务', '/api/TasksQz/put', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(40, b'0', '开启任务', '/api/TasksQz/StartJob', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(41, b'0', '停止任务', '/api/TasksQz/StopJob', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0),
	(42, b'0', '重启任务', '/api/TasksQz/ReCovery', NULL, NULL, NULL, NULL, NULL, 0, NULL, b'0', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 0);
/*!40000 ALTER TABLE `Modules` ENABLE KEYS */;

-- 导出  表 blogTest001.OperateLog 结构
CREATE TABLE IF NOT EXISTS `OperateLog` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Area` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Controller` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Action` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IPAddress` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LogTime` datetime DEFAULT NULL,
  `LoginName` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `UserId` int NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.OperateLog 的数据：~0 rows (大约)
DELETE FROM `OperateLog`;
/*!40000 ALTER TABLE `OperateLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `OperateLog` ENABLE KEYS */;

-- 导出  表 blogTest001.PasswordLib 结构
CREATE TABLE IF NOT EXISTS `PasswordLib` (
  `PLID` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `plURL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plPWD` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plAccountName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plStatus` int DEFAULT NULL,
  `plErrorCount` int DEFAULT NULL,
  `plHintPwd` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plHintquestion` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plCreateTime` datetime DEFAULT NULL,
  `plUpdateTime` datetime DEFAULT NULL,
  `plLastErrTime` datetime DEFAULT NULL,
  `test` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`PLID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='WMBLOG_MSSQL_2';

-- 正在导出表  blogTest001.PasswordLib 的数据：~0 rows (大约)
DELETE FROM `PasswordLib`;
/*!40000 ALTER TABLE `PasswordLib` DISABLE KEYS */;
/*!40000 ALTER TABLE `PasswordLib` ENABLE KEYS */;

-- 导出  表 blogTest001.Permission 结构
CREATE TABLE IF NOT EXISTS `Permission` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsButton` bit(1) NOT NULL,
  `IsHide` bit(1) DEFAULT NULL,
  `IskeepAlive` bit(1) DEFAULT NULL,
  `Func` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `OrderSort` int NOT NULL,
  `Icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Enabled` bit(1) NOT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Pid` int NOT NULL,
  `Mid` int NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Permission 的数据：~82 rows (大约)
DELETE FROM `Permission`;
/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` (`Id`, `Code`, `Name`, `IsButton`, `IsHide`, `IskeepAlive`, `Func`, `OrderSort`, `Icon`, `Description`, `Enabled`, `CreateId`, `CreateBy`, `CreateTime`, `ModifyId`, `ModifyBy`, `ModifyTime`, `IsDeleted`, `Pid`, `Mid`) VALUES
	(1, '/', 'QQ欢迎页', b'0', b'0', b'0', NULL, 0, 'fa-qq', '33', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(2, '-', '用户角色管理', b'0', b'0', b'0', NULL, 0, 'fa-users', '11', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(3, '/User/Roles', '角色管理', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 2, 22),
	(4, '/User/Users', '用户管理', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 2, 7),
	(5, '-', '菜单权限管理', b'0', b'0', b'0', NULL, 0, 'fa-sitemap', NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(6, '/Permission/Module', '接口管理', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 5, 13),
	(7, '/Permission/Permission', '菜单管理', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 5, 17),
	(8, '/Thanks', '致谢页', b'0', b'0', b'0', NULL, 5, 'fa-star ', NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(9, '无', '查询', b'1', b'0', b'0', 'getUsers', 0, NULL, '这个用户页的查询按钮', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 4, 7),
	(10, '-', '报表管理', b'0', b'0', b'0', NULL, 0, 'fa-line-chart', NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(11, '/Form/Charts', '图表', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 10, 0),
	(12, '/Form/Form', '表单', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 10, 0),
	(13, ' ', '新增', b'1', b'0', b'0', 'handleAdd', 0, NULL, '新增用户', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 4, 10),
	(14, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑用户', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 4, 12),
	(15, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除用户', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 4, 11),
	(16, ' ', '查询', b'1', b'0', b'0', 'getRoles', 0, NULL, '查询 角色', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 3, 22),
	(17, ' ', '新增', b'1', b'0', b'0', 'handleAdd', 0, NULL, '新增 角色', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 3, 25),
	(18, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑角色', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 3, 24),
	(19, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除角色', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 3, 23),
	(20, ' ', '查询', b'1', b'0', b'0', 'getModules', 0, NULL, '查询 接口', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 6, 13),
	(21, ' ', '新增', b'1', b'0', b'0', 'handleAdd', 0, NULL, '新增 接口', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 6, 16),
	(22, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑 接口', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 6, 15),
	(23, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除接口', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 6, 14),
	(24, ' ', '查询', b'1', b'0', b'0', 'getPermissions', 0, NULL, '查询 菜单', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 17),
	(25, ' ', '新增', b'1', b'0', b'0', 'handleAdd', 0, NULL, '新增菜单', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 20),
	(26, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑菜单', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 19),
	(27, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除 菜单', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 18),
	(28, '/Tibug/Bugs', 'TiBug', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 42, 26),
	(29, '-', '博客管理', b'0', b'0', b'0', NULL, 0, 'fa-file-word-o', NULL, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(30, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑 tibug ', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 28, 5),
	(31, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除 tibug', b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 28, 6),
	(32, ' ', '查询', b'1', b'0', b'0', 'getBugs', 0, NULL, '查询 tibug', b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 28, 26),
	(33, ' ', '菜单树', b'1', b'1', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 21),
	(34, '/Permission/Assign', '权限分配', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 5, 0),
	(35, ' ', '保存权限', b'1', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 34, 28),
	(36, ' ', '左侧导航', b'1', b'1', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 7, 29),
	(37, '-', '测试页面管理', b'0', b'0', b'0', NULL, 0, 'fa-flask', NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(38, '/TestShow/TestOne', '测试页面1', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 37, 0),
	(39, '/TestShow/TestTwo', '测试页面2', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 37, 0),
	(40, '/I18n/index', '国际化', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 41, 0),
	(41, '-', '多语言管理', b'0', b'0', b'0', NULL, 0, 'fa-language', NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(42, '-', '问题管理', b'0', b'0', b'0', NULL, 0, 'fa-bug', NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(43, '/Blog/Blogs', '博客', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 29, 27),
	(44, '-', '多级路由', b'0', b'0', b'0', NULL, 0, 'fa-sort-amount-asc', NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(45, '-', 'Menu-1', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 44, 0),
	(46, '/Recursion/Menu_1/Menu_1_2', 'Menu-1-2', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 45, 0),
	(47, '-', 'Menu-1-1', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 45, 0),
	(48, '/Recursion/Menu_1/Menu_1_1/Menu_1_1_1', 'Menu-1-1-1', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 47, 0),
	(49, '/Recursion/Menu_1/Menu_1_1/Menu_1_1_2', 'Menu-1-1-2', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 47, 0),
	(50, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(51, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(52, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(53, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(54, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(55, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(56, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(57, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(58, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(59, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(60, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(61, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(62, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(63, 's', 's', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'0', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 0, 0),
	(64, ' ', '删除', b'1', b'0', b'0', 'handleDel', 0, NULL, '删除博客按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 43, 32),
	(65, '-', '日志管理', b'0', b'0', b'0', NULL, 0, 'fa-diamond', NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(66, '/Logs/Index', '全部日志', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 65, 33),
	(67, '/Blog/Detail/:id', '博客详情', b'0', b'1', b'0', NULL, 0, NULL, NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 29, 0),
	(68, '-', '系统管理', b'0', b'0', b'0', NULL, 1, 'el-icon-s-operation', NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(69, '/System/My', '个人中心', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 68, 0),
	(70, ' ', '查询', b'1', b'0', b'0', NULL, 0, NULL, 'Agent 代理的查询接口', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 69, 34),
	(71, ' ', '查询', b'1', b'0', b'0', NULL, 0, NULL, '查询 部门 Department get', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'1', 69, 35),
	(72, ' ', '查询', b'1', b'0', b'0', NULL, 0, NULL, NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 69, 36),
	(73, ' ', '查询', b'1', b'0', b'0', 'getBlogs', 0, NULL, '查询博客按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 43, 27),
	(74, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑博客按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 43, 27),
	(75, '-', '任务调度', b'0', b'0', b'0', NULL, 1, 'fa-history', NULL, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 0, 0),
	(76, '/Task/QuartzJob', '任务列表', b'0', b'0', b'0', NULL, 0, NULL, NULL, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 75, 37),
	(77, ' ', '查询', b'1', b'0', b'0', 'getTasks', 0, NULL, '查询任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 37),
	(78, ' ', '添加', b'1', b'0', b'0', 'handleAdd', 0, NULL, '添加任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 38),
	(79, ' ', '编辑', b'1', b'0', b'0', 'handleEdit', 0, NULL, '编辑任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 39),
	(80, ' ', '开启', b'1', b'0', b'0', 'handleStartJob', 0, NULL, '开启任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 40),
	(81, ' ', '暂停', b'1', b'0', b'0', 'handleStopJob', 0, NULL, '暂停任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 41),
	(82, ' ', '重启', b'1', b'0', b'0', 'handleReCoveryJob', 0, NULL, '重启任务按钮', b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', b'0', 76, 42);
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;

-- 导出  表 blogTest001.Role 结构
CREATE TABLE IF NOT EXISTS `Role` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `OrderSort` int NOT NULL,
  `Enabled` bit(1) NOT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(255) DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Role 的数据：~29 rows (大约)
DELETE FROM `Role`;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` (`Id`, `IsDeleted`, `Name`, `Description`, `OrderSort`, `Enabled`, `CreateId`, `CreateBy`, `CreateTime`, `ModifyId`, `ModifyBy`, `ModifyTime`) VALUES
	(1, b'0', 'Admin', '普通管理', 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(2, b'0', 'System', '系统管理', 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(3, b'0', 'Tibug', 'tibug系统管理', 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(4, b'0', 'SuperAdmin', '超级管理', 0, b'1', 23, 'blogadmin', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(5, b'1', 'AdminTest', NULL, 1, b'1', 18, '提bug账号', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(6, b'0', 'AdminTest', '测试管理', 1, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(7, b'0', 'AdminTest2', '测试管理2', 1, b'1', 23, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(8, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(9, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(10, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(11, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(12, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(13, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(14, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(15, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(16, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(17, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(18, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(19, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(20, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(21, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(22, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(23, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(24, b'1', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(25, b'0', 'sss', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(26, b'0', '213', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(27, b'0', '抬头填', NULL, 1, b'1', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(28, b'0', 'hello1', '测试 常用 get post put 请求', 1, b'1', 12, '后台总管理员', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00'),
	(29, b'1', '55', '555', 1, b'0', 39, 'Kawhi', '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;

-- 导出  表 blogTest001.RoleModulePermission 结构
CREATE TABLE IF NOT EXISTS `RoleModulePermission` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  `RoleId` int NOT NULL,
  `ModuleId` int NOT NULL,
  `PermissionId` int DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.RoleModulePermission 的数据：~120 rows (大约)
DELETE FROM `RoleModulePermission`;
/*!40000 ALTER TABLE `RoleModulePermission` DISABLE KEYS */;
INSERT INTO `RoleModulePermission` (`Id`, `IsDeleted`, `CreateId`, `CreateBy`, `CreateTime`, `ModifyId`, `ModifyBy`, `ModifyTime`, `RoleId`, `ModuleId`, `PermissionId`) VALUES
	(1, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 1, 0),
	(2, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 2, 0),
	(3, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 3, 0),
	(4, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 4, 0),
	(5, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 2, 4, 0),
	(6, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 5, 30),
	(7, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 6, 31),
	(8, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 7, 9),
	(9, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 26, 28),
	(10, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 7, 3),
	(11, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 7, 9),
	(12, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 10, 13),
	(13, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 12, 14),
	(14, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 11, 15),
	(15, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 2),
	(16, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 22, 4),
	(17, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 22, 16),
	(18, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 25, 17),
	(19, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 24, 18),
	(20, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 23, 19),
	(21, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 1),
	(22, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 5),
	(23, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 13, 6),
	(24, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 13, 20),
	(25, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 16, 21),
	(26, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 15, 22),
	(27, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 14, 23),
	(28, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 17, 7),
	(29, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 17, 24),
	(30, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 20, 25),
	(31, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 19, 26),
	(32, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 18, 27),
	(33, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 8),
	(34, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 10),
	(35, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 11),
	(36, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 12),
	(37, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 26, 28),
	(38, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 5, 30),
	(39, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 6, 31),
	(40, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 27, 29),
	(41, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 26, 32),
	(42, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 1),
	(43, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 8),
	(44, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 10),
	(45, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 11),
	(46, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 12),
	(47, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 27, 29),
	(48, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 2),
	(49, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 7, 3),
	(50, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 22, 4),
	(51, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 5),
	(52, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 13, 6),
	(53, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 17, 7),
	(54, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 26, 28),
	(55, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 28, 34),
	(56, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 21, 33),
	(57, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 7, 9),
	(58, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 22, 16),
	(59, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 13, 20),
	(60, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 17, 24),
	(61, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 26, 32),
	(62, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 28, 35),
	(63, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 21, 33),
	(64, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 29, 36),
	(65, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 34),
	(66, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 0, 1),
	(67, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 0, 2),
	(68, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 7, 4),
	(69, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 0, 10),
	(70, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 0, 12),
	(71, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 0, 8),
	(72, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 22, 16),
	(73, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 37),
	(74, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 38),
	(75, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 39),
	(76, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 40),
	(77, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 40),
	(78, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 37),
	(79, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 38),
	(80, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 39),
	(81, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 41),
	(82, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 41),
	(83, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 42),
	(84, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 42),
	(85, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 0, 42),
	(86, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 43),
	(87, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 43),
	(88, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 44),
	(89, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 45),
	(90, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 46),
	(91, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 47),
	(92, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 48),
	(93, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 32, 64),
	(94, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 65),
	(95, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 33, 66),
	(96, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 65),
	(97, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 33, 66),
	(98, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 67),
	(99, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 67),
	(100, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 68),
	(101, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 0, 69),
	(102, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 68),
	(103, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 0, 69),
	(104, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 29, 36),
	(105, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 29, 36),
	(106, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 27, 33),
	(107, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 36, 72),
	(108, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 0, 1),
	(109, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 0, 2),
	(110, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 22, 3),
	(111, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 7, 4),
	(112, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 22, 16),
	(113, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 7, 9),
	(114, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 25, 17),
	(115, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 10, 13),
	(116, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 21, 33),
	(117, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 28, 29, 36),
	(118, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 32, 73),
	(119, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 6, 32, 73),
	(120, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 27, 74);
/*!40000 ALTER TABLE `RoleModulePermission` ENABLE KEYS */;

-- 导出  表 blogTest001.sysUserInfo 结构
CREATE TABLE IF NOT EXISTS `sysUserInfo` (
  `uID` int NOT NULL AUTO_INCREMENT,
  `uLoginName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uLoginPWD` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uRealName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uStatus` int NOT NULL,
  `uRemark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uCreateTime` datetime NOT NULL,
  `uUpdateTime` datetime NOT NULL,
  `uLastErrTime` datetime NOT NULL,
  `uErrorCount` int NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sex` int DEFAULT NULL,
  `age` int DEFAULT NULL,
  `birth` datetime DEFAULT NULL,
  `addr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdIsDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.sysUserInfo 的数据：~39 rows (大约)
DELETE FROM `sysUserInfo`;
/*!40000 ALTER TABLE `sysUserInfo` DISABLE KEYS */;
INSERT INTO `sysUserInfo` (`uID`, `uLoginName`, `uLoginPWD`, `uRealName`, `uStatus`, `uRemark`, `uCreateTime`, `uUpdateTime`, `uLastErrTime`, `uErrorCount`, `name`, `sex`, `age`, `birth`, `addr`, `tdIsDelete`) VALUES
	(1, 'laozhang', '2AEFC34200A294A3CC7DB81B43A81873', '老张', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, '老张的哲学', 1, 0, '2019-01-01 00:00:00', NULL, b'0'),
	(2, 'laoli', '2AEFC34200A294A3CC7DB81B43A81873', 'laoli', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 1, 0, '2019-01-01 00:00:00', NULL, b'0'),
	(3, 'user', '2AEFC34200A294A3CC7DB81B43A81873', 'userli', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, '广告', 1, 0, '2019-01-01 00:00:00', NULL, b'0'),
	(4, 'admins', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'0'),
	(5, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(6, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(7, 'tibug', 'BB1C0516F0F4469549CD4A95833A78E5', '提bug账号', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 1, 0, '2019-01-01 00:00:00', NULL, b'0'),
	(8, 'test', '098F6BCD4621D373CADE4E832627B4F6', '后台测试1号', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, '测试是', 1, 3, '2019-01-01 00:00:00', NULL, b'0'),
	(9, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(10, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(11, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(12, 'blogadmin', '3FACF26687DAB7254848976256EDB56F', '后台总管理员', 0, 't15', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 1, 10, '2019-01-01 00:00:00', NULL, b'0'),
	(13, 'test2', 'AD0234829205B9033196BA818F7A872B', '后台测试2号', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 12, '2019-01-01 00:00:00', '北京市', b'0'),
	(14, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(15, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(16, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(17, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(18, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(19, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(20, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(21, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(22, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(23, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(24, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(25, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(26, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(27, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(28, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(29, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(30, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(31, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(32, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(33, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(34, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(35, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(36, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(37, 'xx', '2AEFC34200A294A3CC7DB81B43A81873', 'admins', 0, NULL, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 0, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(38, '99', 'AC627AB1CCBDB62EC96E702F7F6425B', '99', 0, 'blogadmin', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, -1, 0, '2019-01-01 00:00:00', NULL, b'1'),
	(39, 'Kawhi', '96FEE3FD714358658BFB881A4E1642BE', 'Kawhi 测试员', 0, 'blogadmin', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 1, 18, '2019-01-01 00:00:00', NULL, b'0');
/*!40000 ALTER TABLE `sysUserInfo` ENABLE KEYS */;

-- 导出  表 blogTest001.TasksQz 结构
CREATE TABLE IF NOT EXISTS `TasksQz` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `JobGroup` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Cron` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `AssemblyName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ClassName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RunTimes` int NOT NULL,
  `BeginTime` datetime NOT NULL,
  `EndTime` datetime NOT NULL,
  `TriggerType` int NOT NULL,
  `IntervalSecond` int NOT NULL,
  `IsStart` bit(1) NOT NULL,
  `JobParams` varchar(255) NOT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.TasksQz 的数据：~11 rows (大约)
DELETE FROM `TasksQz`;
/*!40000 ALTER TABLE `TasksQz` DISABLE KEYS */;
INSERT INTO `TasksQz` (`Id`, `Name`, `JobGroup`, `Cron`, `AssemblyName`, `ClassName`, `Remark`, `RunTimes`, `BeginTime`, `EndTime`, `TriggerType`, `IntervalSecond`, `IsStart`, `JobParams`, `IsDeleted`, `CreateTime`) VALUES
	(1, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：1，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(2, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：2，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(3, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：3，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(4, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：4，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(5, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：5，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(6, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：6，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(7, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：7，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(8, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：8，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(9, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:51】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：9，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(10, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：10，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00'),
	(11, '博客管理', '博客测试组', '*/5 * * * * ?', 'Blog.Core.Tasks', 'Job_Blogs_Quartz', '【2020/12/22 15:32:55】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:50】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:45】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:40】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:35】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:30】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:25】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:20】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:15】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1<br>【2020/12/22 15:32:10】执行任务【Id：11，组别：博客测试组】【执行成功】:博客数1', 151, '2019-01-01 00:00:00', '2022-01-01 00:00:00', 1, 0, b'1', '1', b'0', '2019-01-01 00:00:00');
/*!40000 ALTER TABLE `TasksQz` ENABLE KEYS */;

-- 导出  表 blogTest001.TestMuchTableResult 结构
CREATE TABLE IF NOT EXISTS `TestMuchTableResult` (
  `moduleName` varchar(255) NOT NULL,
  `permName` varchar(255) NOT NULL,
  `rid` int NOT NULL,
  `mid` int NOT NULL,
  `pid` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.TestMuchTableResult 的数据：~0 rows (大约)
DELETE FROM `TestMuchTableResult`;
/*!40000 ALTER TABLE `TestMuchTableResult` DISABLE KEYS */;
/*!40000 ALTER TABLE `TestMuchTableResult` ENABLE KEYS */;

-- 导出  表 blogTest001.Topic 结构
CREATE TABLE IF NOT EXISTS `Topic` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `tLogo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tDetail` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tAuthor` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tSectendDetail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tIsDelete` bit(1) NOT NULL,
  `tRead` int NOT NULL,
  `tCommend` int NOT NULL,
  `tGood` int NOT NULL,
  `tCreatetime` datetime NOT NULL,
  `tUpdatetime` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.Topic 的数据：~1 rows (大约)
DELETE FROM `Topic`;
/*!40000 ALTER TABLE `Topic` DISABLE KEYS */;
INSERT INTO `Topic` (`Id`, `tLogo`, `tName`, `tDetail`, `tAuthor`, `tSectendDetail`, `tIsDelete`, `tRead`, `tCommend`, `tGood`, `tCreatetime`, `tUpdatetime`) VALUES
	(1, '/Upload/20180626/95445c8e288e47e3af7a180b8a4cc0c7.jpg', '《罗马人的故事》', '这是一个荡气回肠的故事', 'Laozhang', NULL, b'0', 0, 0, 0, '2019-01-01 00:00:00', '2019-01-01 00:00:00');
/*!40000 ALTER TABLE `Topic` ENABLE KEYS */;

-- 导出  表 blogTest001.TopicDetail 结构
CREATE TABLE IF NOT EXISTS `TopicDetail` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `tdLogo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdContent` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdDetail` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdSectendDetail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tdIsDelete` bit(1) NOT NULL,
  `tdRead` int NOT NULL,
  `tdCommend` int NOT NULL,
  `tdGood` int NOT NULL,
  `tdCreatetime` datetime NOT NULL,
  `tdUpdatetime` datetime NOT NULL,
  `tdTop` int NOT NULL,
  `tdAuthor` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TopicId` int NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.TopicDetail 的数据：~1 rows (大约)
DELETE FROM `TopicDetail`;
/*!40000 ALTER TABLE `TopicDetail` DISABLE KEYS */;
INSERT INTO `TopicDetail` (`Id`, `tdLogo`, `tdName`, `tdContent`, `tdDetail`, `tdSectendDetail`, `tdIsDelete`, `tdRead`, `tdCommend`, `tdGood`, `tdCreatetime`, `tdUpdatetime`, `tdTop`, `tdAuthor`, `TopicId`) VALUES
	(1, NULL, '第一章　罗马的诞生 第一节　传说的年代', '<p>第一节　传说的年代</p><p>每个民族都有自己的神话传说。大概希望知道本民族的来源是个很自然的愿望吧。但这是一个难题，因为这几乎不可能用科学的方法来解释清楚。不过所有的民族都没有这样的奢求。他们只要有一个具有一定的条理性，而又能振奋其民族精神的浪漫故事就行，别抬杠，象柏杨那样将中国的三皇五帝都来个科学分析，来评论他们的执政之优劣是大可不必的。</p><p>对於罗马人，他们有一个和特洛伊城的陷落相关的传说。</p><p>位於小亚细亚西岸的繁荣的城市特洛伊，在遭受了阿加美农统帅的希腊联军的十年围攻之後，仍未陷落。希腊联军於是留下一个巨大的木马後假装撤兵。特洛伊人以为那是希腊联军留给自己的礼物，就将它拉入城内。</p><p>当庆祝胜利的狂欢结束，特洛伊人满怀对明日的和平生活的希望熟睡後，藏在木马内的希腊士兵一个又一个地爬了出来。就在这天夜里，特洛伊城便在火光和叫喊中陷落了。全城遭到大屠杀 ，幸免於死的人全都沦为奴隶。混乱之中只有特洛伊国王的驸马阿伊尼阿斯带着老父，儿子等数人在女神维娜斯的帮助下成功地逃了出来。这驸马爷乃是女神维娜斯与凡人男子之间的儿子，女神维娜斯不忍心看着自己的儿子被希腊士兵屠杀 。</p>但纷争又开始了，勒莫跳过了罗莫路为表示势力范围而挖的沟。对於这种侵犯他人权力的行为，罗莫路大义灭亲地在自己兄弟的後脑上重重地来了一锄头，勒莫便被灭了。</p><p></p><p>於是这城便以罗莫路的名字命名为罗马，这就是公元前731年4月21日的事了，到现在这天仍是意大利的节日，罗马人会欢天喜地的庆祝罗莫路杀了自己的…不，是庆祝罗马建城。王位当然也得由罗莫路来坐，一切问题都没了。这时四年一度的奥林匹克运动会在希腊已经开了六回，罗马也从传说的时代走出，近入了历史时代。</p><p><br></p>', '标题', NULL, b'0', 8, 0, 0, '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0, NULL, 1);
/*!40000 ALTER TABLE `TopicDetail` ENABLE KEYS */;

-- 导出  表 blogTest001.UserRole 结构
CREATE TABLE IF NOT EXISTS `UserRole` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IsDeleted` bit(1) DEFAULT NULL,
  `CreateId` int DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifyId` int DEFAULT NULL,
  `ModifyBy` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ModifyTime` datetime DEFAULT NULL,
  `UserId` int NOT NULL,
  `RoleId` int NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  blogTest001.UserRole 的数据：~11 rows (大约)
DELETE FROM `UserRole`;
/*!40000 ALTER TABLE `UserRole` DISABLE KEYS */;
INSERT INTO `UserRole` (`Id`, `IsDeleted`, `CreateId`, `CreateBy`, `CreateTime`, `ModifyId`, `ModifyBy`, `ModifyTime`, `UserId`, `RoleId`) VALUES
	(1, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 4, 1),
	(2, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 3, 2),
	(3, b'0', NULL, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 7, 3),
	(4, b'0', 23, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 12, 4),
	(5, b'0', 1, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 2),
	(6, b'0', 1, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 1, 1),
	(7, b'0', 13, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 2, 1),
	(8, b'0', 19, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 8, 6),
	(9, b'0', 24, NULL, '2019-01-01 00:00:00', NULL, NULL, '2019-01-01 00:00:00', 13, 7),
	(10, b'0', 0, NULL, '2019-01-01 00:00:00', NULL, NULL, NULL, 0, 0),
	(11, b'0', 39, NULL, '2019-01-01 00:00:00', NULL, NULL, NULL, 39, 28);
/*!40000 ALTER TABLE `UserRole` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
